import React, { Component } from 'react';

import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { persistor, store } from './assets/store/storeIndex';
import Navigation from './navigation';

export default class App extends Component {
  renderingItem = () => (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <Navigation />
      </PersistGate>
    </Provider>
  )

  render() {
    return (
      this.renderingItem()
    );
  }
}
