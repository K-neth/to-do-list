import { createStackNavigator, createAppContainer } from 'react-navigation';

import Home from './assets/container/Home-Container';
import Task from './assets/container/Tasks-Container';
import User from './assets/container/User-Container';
import UserDetail from './assets/container/UserDetail-Container';

const Navigator = createStackNavigator({
  Task: {
    screen: Task,
    navigationOptions: () => ({
    }),
  },
  Home: {
    screen: Home,
    navigationOptions: () => ({
      header: null,
    }),
  },
  User: {
    screen: User,
    navigationOptions: () => ({
    }),
  },
  UserDetail: {
    screen: UserDetail,
    navigationOptions: () => ({
    }),
  },
}, {
  initialRouteName: 'Home',
});
const appContainer = createAppContainer(Navigator);

export default appContainer;
