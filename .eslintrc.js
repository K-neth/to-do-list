module.exports = {
  "globals": {
    "fetch": false
  },
  "env": {
    "jest": true
  },
  "parser": "babel-eslint",
  "extends": "airbnb",
  "rules": {
    "react/jsx-filename-extension": [1, { "extensions": [".js", ".jsx"] }],
    "react/destructuring-assignment": [0, 'never'],
    "import/no-extraneous-dependencies": ["error", {"devDependencies": true}]
  }
};
