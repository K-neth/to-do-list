/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from 'react';
import {
  StyleSheet, Text, View, FlatList, TouchableHighlight,
  Image,

} from 'react-native';
import moment from 'moment';
import PropTypes from 'prop-types';
import TaskList from './TaskList';

const user = require('../img/manager-white.png');

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  upperContainer: {
    flex: 1,
    paddingLeft: 25,
    paddingTop: 100,
    paddingRight: 25,
    paddingBottom: 30,
    backgroundColor: '#6666cc',
  },
  bottomContainer: {
    flex: 1,
    paddingLeft: 25,
    paddingTop: 100,
    paddingRight: 25,
    paddingBottom: 30,
    backgroundColor: '#cc6666',
  },
  secondContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: 'white',
    borderRadius: 10,
  },
  thirdContainer: {
    padding: 20,
  },
  containerbtn: {
    flexDirection: 'row',
    padding: 5,
    borderRadius: 10,
    justifyContent: 'flex-end',
    backgroundColor: 'white',
  },
  txtdate: {
    fontSize: 25,
    color: '#6666cc',
    fontWeight: 'bold',
  },
  txtdate2: {
    fontSize: 25,
    color: '#6666cc',
  },
  txtTotalTask: {
    marginTop: 12,
  },
  txtNewTask: {
    justifyContent: 'center',
    paddingTop: 10,
    fontSize: 40,
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  btnNewTask: {
    height: 50,
    width: 50,
    justifyContent: 'flex-end',
    borderWidth: 0,
    elevation: 10,
    backgroundColor: '#ff6666',
    borderRadius: 100,
    marginRight: 12,
    marginLeft: 12,
  },
  btnUser: {
    height: 50,
    width: 50,
    justifyContent: 'flex-start',
    alignSelf: 'flex-start',
    borderWidth: 0,
    elevation: 10,
    backgroundColor: '#ff6666',
    borderRadius: 100,
    marginRight: 12,
    marginLeft: 12,
  },
  txtTaskT: {
    textDecorationLine: 'line-through',
    width: 250,
    opacity: 0.5,
    paddingTop: 5,
    textAlign: 'left',
  },
  txtTaskF: {
    textDecorationLine: 'none',
    width: 250,
    paddingTop: 5,
    textAlign: 'left',
  },
  txtTaskTime: {
    opacity: 0.5,
    paddingTop: 10,
    paddingRight: 50,
    fontSize: 10,
  },
  txtTaskTime2: {
    opacity: 0.5,
    paddingTop: 10,
    paddingRight: 10,
    fontSize: 10,
  },
  listContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'stretch',
    justifyContent: 'space-between',
    paddingTop: 10,
    borderRadius: 10,
    backgroundColor: 'white',
  },
  borderContainer: {
    borderWidth: 0,
    borderRadius: 10,
    elevation: 10,
    backgroundColor: 'white',
    justifyContent: 'center',
    marginTop: 80,
    marginBottom: 80,
    height: 650,
    position: 'absolute',
    alignSelf: 'center',
  },
  separator: {
    height: 0.8,
    backgroundColor: 'black',
    width: 250,
    marginTop: 25,
  },
  txtUser: {
    marginTop: 8,
    marginLeft: 8,
  },
});

export default class Home extends Component {
  state = {
    data: [],
  };

  renderSeparator = () => (
    <View
      style={{
        backgroundColor: 'black',
        height: 0.5,
      }}
    />
  );

  submitNewTask = (newTask) => {
    this.setState(prevState => (
      {
        data: [...prevState.data, newTask],
      }
    ));
  }

  onTodoStatusChange = (id) => {
    this.props.onTodoStatusChange(id);
  }


  render() {
    if ((this.props.navigation.getParam('data')) !== null) {
      return (
        <View style={styles.mainContainer}>
          <View style={styles.upperContainer} />
          <View style={styles.borderContainer}>
            <View style={styles.secondContainer}>
              <View style={styles.thirdContainer}>
                <Text style={styles.txtdate2}>
                  <Text style={styles.txtdate}>
                    {`${moment().format('dddd')}, `}
                  </Text>
                  { moment().format('Do') }
                </Text>
                <Text>{moment().format('MMMM')}</Text>
              </View>
              <View style={styles.thirdContainer}>
                <Text style={styles.txtTotalTask}>
                  <Text style={{ fontWeight: 'bold' }}>
                    {`${this.props.homes.length} `}
                  </Text>
                  Tasks
                </Text>
              </View>
            </View>
            <View style={styles.containerbtn}>
              <TouchableHighlight
                style={styles.btnUser}
                onPress={() => this.props.navigation.navigate('User')}
              >
                <Image
                  style={styles.txtUser}
                  source={user}
                />
              </TouchableHighlight>
              <View style={styles.separator} />
              <TouchableHighlight
                style={styles.btnNewTask}
                onPress={() => this.props.navigation.navigate('Task', { task: this.submitNewTask })}
              >
                <Text style={styles.txtNewTask}>+</Text>
              </TouchableHighlight>
            </View>
            <FlatList
              ItemSeparatorComponent={this.renderSeparator}
              data={this.props.homes}
              renderItem={
                ({ item }) => <TaskList data={item} ondataStatusChange={this.onTodoStatusChange} />
              }
              keyExtractor={item => item.id.toString()}
            />
          </View>
          <View style={styles.bottomContainer} />
        </View>
      );
    }
    return null;
  }
}

const todoShape = PropTypes.shape({
  id: PropTypes.string,
  taskName: PropTypes.string,
  taskdueDate: PropTypes.string,
  taskdueTime: PropTypes.string,
  checked: PropTypes.bool,
});

Home.propTypes = {
  homes: PropTypes.arrayOf(todoShape).isRequired,
  onTodoStatusChange: PropTypes.func.isRequired,
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
    getParam: PropTypes.func,
  }),
};

Home.defaultProps = {
  navigation: {
    navigate: () => {},
    state: () => {},
    getParam: () => {},
  },
};
