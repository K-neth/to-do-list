/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet, Text, View,
} from 'react-native';
import PropTypes from 'prop-types';

const styles = StyleSheet.create({
  upperContainer: {
    flex: 1,
    paddingLeft: 25,
    paddingTop: 100,
    paddingRight: 25,
    paddingBottom: 30,
    backgroundColor: '#6666cc',
  },
  bottomContainer: {
    flex: 1,
    paddingLeft: 25,
    paddingTop: 100,
    paddingRight: 25,
    paddingBottom: 30,
    backgroundColor: '#cc6666',
  },
  buttonContainer: {
    borderWidth: 0,
    borderRadius: 10,
    elevation: 10,
    backgroundColor: 'white',
    justifyContent: 'flex-start',
    marginTop: 60,
    marginBottom: 80,
    height: 450,
    position: 'absolute',
    alignSelf: 'center',
    alignItems: 'center',
  },
  txtTaskF: {
    textDecorationLine: 'none',
    width: 350,
    marginLeft: 20,
    textAlign: 'left',
  },
  listContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'stretch',
    justifyContent: 'space-between',
    paddingTop: 10,
    borderRadius: 10,
    backgroundColor: 'white',
  },
});

export default class UserDetail extends Component {
  async componentWillMount() {
    const detail = this.props.navigation.getParam('detail');
    this.onLoad(detail);
  }

  onLoad = (item) => {
    this.props.onLoad(item);
  };

  renderName = () => {
    if (this.props.userdetails.detail) {
      return `Name: ${this.props.userdetails.detail.name}`;
    }
    return null;
  }

  renderUsername = () => {
    if (this.props.userdetails.detail) {
      return `Username: ${this.props.userdetails.detail.username}`;
    }
    return null;
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={styles.upperContainer} />
        <View style={styles.bottomContainer} />
        <View style={styles.buttonContainer}>
          <Text style={styles.txtTaskF}>
            {this.renderName()}
          </Text>
          <Text style={styles.txtTaskF}>
            {this.renderUsername()}
          </Text>
        </View>
      </View>
    );
  }
}

const todoShape = PropTypes.shape({
  name: PropTypes.string,
  username: PropTypes.string,
});

UserDetail.propTypes = {
  userdetails: PropTypes.objectOf(todoShape).isRequired,
  onLoad: PropTypes.func.isRequired,
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
    getParam: PropTypes.func,
    goBack: PropTypes.func,
  }),
};

UserDetail.defaultProps = {
  navigation: { navigate: () => {} },
};
