/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet, Text, View, FlatList,
  ActivityIndicator, TouchableWithoutFeedback,
  TextInput, TouchableHighlight, Image,
} from 'react-native';
import PropTypes from 'prop-types';

const searchimg = require('../img/musica-searcher.png');

const styles = StyleSheet.create({
  upperContainer: {
    flex: 1,
    paddingLeft: 25,
    paddingTop: 100,
    paddingRight: 25,
    paddingBottom: 30,
    backgroundColor: '#6666cc',
  },
  bottomContainer: {
    flex: 1,
    paddingLeft: 25,
    paddingTop: 100,
    paddingRight: 25,
    paddingBottom: 30,
    backgroundColor: '#cc6666',
  },
  buttonContainer: {
    width: 430,
    borderWidth: 0,
    borderRadius: 10,
    elevation: 10,
    backgroundColor: 'white',
    justifyContent: 'flex-start',
    flex: 1,
    alignSelf: 'center',
    alignItems: 'center',
  },
  searchContainer: {
    width: 430,
    borderRadius: 10,
    elevation: 10,
    backgroundColor: 'white',
    justifyContent: 'flex-start',
    height: 100,
    flex: 1,
    alignSelf: 'center',
    alignItems: 'center',
  },
  txtTaskF: {
    textDecorationLine: 'none',
    fontWeight: 'bold',
    width: 350,
    marginLeft: 45,
    textAlign: 'left',
    paddingBottom: 10,
  },
  listContainer: {
    flex: 1,
    marginLeft: 40,
    marginRight: 40,
    flexDirection: 'row',
    alignItems: 'stretch',
    justifyContent: 'space-between',
    paddingTop: 10,
    borderRadius: 10,
    borderWidth: 3,
    borderColor: 'blue',
    backgroundColor: 'white',
  },
  detailContainer: {
    flex: 1,
    alignItems: 'stretch',
  },
  avatar: {
    fontSize: 25,
    height: 50,
    width: 50,
    justifyContent: 'flex-start',
    alignSelf: 'flex-start',
    marginTop: 5,
    elevation: 10,
    backgroundColor: '#ff6666',
    borderRadius: 100,
    marginLeft: 10,
    paddingLeft: 15,
    paddingTop: 5,
  },
  searchBar: {
    elevation: 1,
    borderWidth: 3,
    borderColor: 'blue',
    borderRadius: 10,
    width: 350,
    marginBottom: 20,
    backgroundColor: 'red',
    fontWeight: 'bold',
    fontSize: 15,
    alignItems: 'center',
  },
  txtTitle: {
    alignSelf: 'stretch',
    textAlign: 'left',
    marginBottom: 20,
    marginRight: 20,
    marginTop: 20,
    elevation: 100,
    fontSize: 24,
    color: 'blue',
    fontWeight: 'bold',
    width: 300,
  },
  image: {
    marginTop: 20,
  },
});

export default class User extends Component {
  state = {
    isLoading: true,
    refreshing: false,
    search: '',
    visibility: false,
  };

  async componentDidMount() {
    if (this.props.users.length === 0) {
      const userApiCall = await fetch('https://jsonplaceholder.typicode.com/users');
      const user = await userApiCall.json();
      this.setState({ isLoading: false, refreshing: false });
      user.forEach((item) => {
        this.AddUsers(
          item,
        );
      });
    }
    this.setState({ isLoading: false, refreshing: false });
    return this.props.users;
  }

  onRefresh = () => {
    this.setState({
      refreshing: true,
    });
  };

  setVisibility(visible) {
    this.setState({
      visibility: visible,
    });
  }

  AddUsers = (item) => {
    this.props.AddUsers(item);
  };

  renderSeparator = () => (
    <View
      style={{
        height: 0.5,
        paddingTop: 10,
        paddingBottom: 10,
      }}
    />
  );

  renderItem = ({ item }) => (
    <View>
      <TouchableWithoutFeedback
        onPress={() => {
          this.props.navigation.navigate('UserDetail', { detail: item });
        }}
      >
        <View style={styles.listContainer}>
          <Text style={styles.avatar}>
            {item.name.charAt(0)}
          </Text>
          <Text style={styles.txtTaskF}>
            {`Name: ${item.name}`}
            {'\n'}
            {`Username: ${item.username}`}
            {'\n'}
            {`Address: ${item.address.city}`}
          </Text>
        </View>
      </TouchableWithoutFeedback>
    </View>
  )

  render() {
    const { isLoading } = this.state;
    const searchList = this.props.users.filter(user => (user.name.includes(this.state.search)));
    if (!isLoading) {
      return (
        <View style={{ flex: 1 }}>
          <View style={styles.buttonContainer}>
            <View style={{ flexDirection: 'row' }}>
              <Text style={styles.txtTitle}>Contacts</Text>
              <TouchableHighlight
                style={styles.image}
                onPress={() => {
                  this.setVisibility(!this.state.visibility);
                }}
              >
                <Image
                  source={searchimg}
                />
              </TouchableHighlight>
            </View>
            <View>
              {this.state.visibility ? (
                <TextInput
                  style={styles.searchBar}
                  placeholder="Input to Search"
                  returnKeyLabel="search"
                  onChangeText={(search) => {
                    this.setState({ search });
                  }}
                />
              ) : null}
            </View>
            <FlatList
              ItemSeparatorComponent={this.renderSeparator}
              data={searchList}
              extraData={this.state}
              renderItem={this.renderItem}
              keyExtractor={item => item.name}
              refreshing={this.state.refreshing}
              onRefresh={this.onRefresh}
            />
          </View>
        </View>
      );
    }
    return <ActivityIndicator />;
  }
}

const todoShape = PropTypes.shape({
  name: PropTypes.string,
  username: PropTypes.string,
  address: PropTypes.shape({
    city: PropTypes.string,
    street: PropTypes.string,
  }),
});

User.propTypes = {
  users: PropTypes.arrayOf(todoShape).isRequired,
  AddUsers: PropTypes.func.isRequired,
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
    getParam: PropTypes.func,
    goBack: PropTypes.func,
  }),
};

User.defaultProps = {
  navigation: { navigate: () => {} },
};
