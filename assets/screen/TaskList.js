import React, { Component } from 'react';
import {
  View, Text, StyleSheet, Switch,
} from 'react-native';
import PropTypes from 'prop-types';
import moment from 'moment';

const styles = StyleSheet.create({
  container: {
    height: 40,
    margin: 5,
    flexDirection: 'row',
  },
  leftColumn: {
    flex: 1,
  },
  rightColumn: {
    flex: 4,
  },
  txtTaskT: {
    textDecorationLine: 'line-through',
    width: 250,
    opacity: 0.5,
    paddingTop: 5,
    textAlign: 'left',
  },
  txtTaskF: {
    textDecorationLine: 'none',
    width: 250,
    paddingTop: 5,
    textAlign: 'left',
  },
  listContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'stretch',
    justifyContent: 'space-between',
    paddingTop: 10,
    borderRadius: 10,
    backgroundColor: 'white',
  },
});

export default class TaskList extends Component {
    onSwitchValueChange = () => {
      this.props.ondataStatusChange(this.props.data.id);
    }

    render() {
      return (
        <View style={styles.listContainer}>
          <Switch value={this.props.data.checked} onValueChange={this.onSwitchValueChange} />
          <Text
            style={this.props.data.checked ? styles.txtTaskT : styles.txtTaskF}
            numberOfLines={3}
          >
            {this.props.data.taskName}
            {'\n'}
          </Text>
          <Text style={styles.txtTaskTime2}>
            {moment(this.props.data.taskdueTime, 'HHmm').format('LT')}
            {'\n'}
            {this.props.data.taskdueDate}
          </Text>
        </View>
      );
    }
}


const dataShape = PropTypes.shape({
  id: PropTypes.string,
  taskName: PropTypes.string,
  taskdueDate: PropTypes.string,
  taskdueTime: PropTypes.string,
  checked: PropTypes.bool,
});

TaskList.propTypes = {
  data: dataShape.isRequired,
  ondataStatusChange: PropTypes.func.isRequired,
};
