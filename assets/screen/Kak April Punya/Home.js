/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable max-len */
import React, { Component } from 'react';
import {
  Dimensions, Text, TextInput, View, TouchableHighlight, StyleSheet, FlatList, CheckBox, DatePickerAndroid, TimePickerAndroid, Alert, Modal,
} from 'react-native';
import moment from 'moment';

const { height, width } = Dimensions.get('window');
const styles = StyleSheet.create({
  textInput: {
    alignSelf: 'stretch',
    height: 40,
  },
  mainContainer: {
    flex: 1,
    padding: 10,
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  container: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 10,
  },
  button: {
    borderRadius: 10,
    borderColor: '#fff',
    backgroundColor: '#f1c40f',
    alignItems: 'center',
    margin: 4,
  },
  buttonText: {
    padding: 10,
    color: 'gray',
  },
  flatList: {
    // backgroundColor: '#fff8dc',
  },
  cardList: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'white',
    padding: 10,
    borderRadius: 2.5,
    margin: 4,
    elevation: 4,
  },
  outerModal: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#00000080',
  },
  innerModal: {
    width: width - 80,
    height: height - 520,
    backgroundColor: '#fff',
    padding: 5,
    borderRadius: 2.5,
  },
});

class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      task: '',
      date: moment(new Date()).subtract(1, 'month').format('YYYYMMDD'),
      time: moment(new Date()).format(),
      addedTask: [],
      modalVisible: false,
    };
  }

      onPressButtonDate = async () => {
        try {
          const {
            action, year, month, day,
          } = await DatePickerAndroid.open({
            date: new Date(),
          });
          if (action === DatePickerAndroid.dateSetAction) {
            if (month < 10 && day < 10) {
              this.setState(() => ({
                date: `${year}0${month}0${day}`,
              }));
            } else if (month < 10) {
              this.setState(() => ({
                date: `${year}0${month}${day}`,
              }));
            } else if (day < 10) {
              this.setState(() => ({
                date: `${year}${month}0${day}`,
              }));
            } else {
              this.setState(() => ({
                date: `${year}${month}${day}`,
              }));
            }
          }
        } catch ({ code, message }) {
          Alert.alert('Cannot open date picker');
        }
      }

      onPressButtonTime = async () => {
        try {
          const {
            action, hour, minute,
          } = await TimePickerAndroid.open({
            hour: 14,
            minute: 0,
            is24Hour: true, // Will display '14'
          });
          if (action !== TimePickerAndroid.dismissedAction) {
            if (hour < 10 && minute < 10) {
              this.setState(() => ({
                time: `${this.state.date}T0${hour}0${minute}`,
              }));
            } else if (hour < 10) {
              this.setState(() => ({
                time: `${this.state.date}T0${hour}${minute}`,
              }));
            } else if (minute < 10) {
              this.setState(() => ({
                time: `${this.state.date}T${hour}0${minute}`,
              }));
            } else {
              this.setState(() => ({
                time: `${this.state.date}T${hour}${minute}`,
              }));
            }
          }
        } catch ({ code, message }) {
          Alert.alert('Cannot open time picker');
        }
      }

      onPressButton = () => {
        if (this.state.task === '' || this.state.date === '' || this.state.time === '') {
          Alert.alert('Task cannot be empty! Due date and time must be set!');
        } else {
          const foundDuplication = this.state.addedTask.find(x => x.taskName === this.state.task
            && x.taskDate === this.state.date
            && x.taskTime === this.state.time);

          if (!foundDuplication) {
            const arrayIndex = this.state.addedTask.length;
            this.setState(() => ({
              task: '',
              date: '',
              time: '',
              addedTask: [...this.state.addedTask, {
                taskName: this.state.task,
                checked: false,
                taskDate: this.state.date,
                taskTime: this.state.time,
                taskID: arrayIndex,
              }],
            }));
          } else {
            Alert.alert('Task is already exist');
          }
        }
        this.setModalVisible(!this.state.modalVisible);
      }

      onCheckedBox = (taskItem) => {
        const newArray = this.state.addedTask.map((arrayItem) => {
          if (arrayItem.taskID === taskItem.taskID) {
            return ({
              taskName: arrayItem.taskName,
              checked: !arrayItem.checked,
              taskDate: arrayItem.taskDate,
              taskTime: arrayItem.taskTime,
              taskID: arrayItem.taskID,
            });
          }
          return arrayItem;
        });
        this.setState({ addedTask: newArray });
      }

      setModalVisible(visible) {
        this.setState({
          modalVisible: visible,
          task: '',
          date: moment(new Date()).subtract(1, 'month').format('YYYYMMDD'),
          time: moment(new Date()).format(),
        });
      }

      handleTextChange = (value) => {
        const task = value;
        this.setState(() => ({
          task,
        }));
      }

      keyExtractor = item => item.taskID;

      renderItem = ({ item }) => (
        <View style={styles.cardList}>
          <CheckBox
            value={item.checked}
            onValueChange={() => this.onCheckedBox(item)}
          />
          <View>
            <Text>{item.taskName}</Text>
            <View style={{ flexDirection: 'row' }}>
              <Text>{moment(`${item.taskTime}`).add(1, 'month').format('LLLL')}</Text>
            </View>
          </View>
        </View>
      )

      render() {
        return (
          <View style={styles.mainContainer}>
            <Modal
              transparent
              animationType="slide"
              onRequestClose={() => {
                this.setModalVisible(!this.state.modalVisible);
              }}
              visible={this.state.modalVisible}
            >
              <View style={styles.outerModal}>
                <View style={styles.innerModal}>
                  <View style={styles.container}>
                    <TextInput
                      style={styles.textInput}
                      placeholder="Type here to add task!"
                      onChangeText={this.handleTextChange}
                      value={this.state.task}
                      autoFocus
                      autoCapitalize="sentences"
                      autoCorrect
                      keyboardType="default"
                    />
                    <View style={styles.buttonContainer}>
                      <TouchableHighlight onPress={this.onPressButtonDate} underlayColor="white">
                        <View>
                          <Text style={styles.buttonText}>{moment(this.state.date).add(1, 'month').format('LL')}</Text>
                        </View>
                      </TouchableHighlight>

                      <TouchableHighlight onPress={this.onPressButtonTime} underlayColor="white">
                        <View>
                          <Text style={styles.buttonText}>{moment(this.state.time).format('LT')}</Text>
                        </View>
                      </TouchableHighlight>

                      <TouchableHighlight onPress={this.onPressButton} underlayColor="white">
                        <View style={styles.button}>
                          <Text style={styles.buttonText}>Add</Text>
                        </View>
                      </TouchableHighlight>
                    </View>
                  </View>
                </View>
              </View>
            </Modal>
            <TouchableHighlight
              style={styles.button}
              onPress={() => { this.setModalVisible(true); }}
            >
              <Text style={styles.buttonText}>Add Task</Text>
            </TouchableHighlight>
            <FlatList
              style={styles.flatList}
              data={this.state.addedTask}
              keyExtractor={this.keyExtractor}
              renderItem={this.renderItem}
            />
          </View>
        );
      }
}

export default Main;
