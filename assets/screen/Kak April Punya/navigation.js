/* eslint-disable import/no-unresolved */

import { createStackNavigator } from 'react-navigation';
import * as Constants from './constants';
import LogIn from './screen/login-screen';
import SplashScreen from './screen/splash-screen';
import MainScreen from './screen/main-screen';
import SolutionScreen from './screen/solution-screen';
import ProductListScreen from './screen/product-list-screen';
import ServiceCalculatorScreen from './screen/service-calculator-screen';
import ManagementDirectionScreen from './screen/management-direction-screen';
import NewsScreen from './screen/news-screen';
import ChallengeInfoScreen from './screen/challenge-info-screen';
import InquiriesScreen from './screen/inquiries-screen';
import FaqScreen from './screen/faq-screen';
import MiiProfileScreen from './screen/mii-profile-screen';
import CatalogDetailsScreen from './screen/catalog-details-screen';

const RootNavigation = createStackNavigator(
  {
    [Constants.NAV_NAME_LOGIN]: {
      screen: LogIn,
      navigationOptions: () => ({
        header: null,
      }),
    },
    [Constants.NAV_NAME_SPLASH]: {
      screen: SplashScreen,
      navigationOptions: () => ({
        header: null,
      }),
      statusBarOptions: { hidden: true },
    },
    [Constants.NAV_NAME_MAIN_PAGE]: {
      screen: MainScreen,
      navigationOptions: () => ({
        header: null,
      }),
    },
    [Constants.NAV_NAME_SOLUTION]: {
      screen: SolutionScreen,
      navigationOptions: () => ({
        header: null,
      }),
    },
    [Constants.NAV_NAME_PRODUCT_LIST]: {
      screen: ProductListScreen,
      navigationOptions: () => ({
        header: null,
      }),
    },
    [Constants.NAV_NAME_SERVICE_CALCULATOR]: {
      screen: ServiceCalculatorScreen,
      navigationOptions: () => ({
        header: null,
      }),
    },
    [Constants.NAV_NAME_MANAGEMENT_DIRECTION]: {
      screen: ManagementDirectionScreen,
      navigationOptions: () => ({
        header: null,
      }),
    },
    [Constants.NAV_NAME_NEWS]: {
      screen: NewsScreen,
      navigationOptions: () => ({
        header: null,
      }),
    },
    [Constants.NAV_NAME_CHALLENGE_INFO]: {
      screen: ChallengeInfoScreen,
      navigationOptions: () => ({
        header: null,
      }),
    },
    [Constants.NAV_NAME_INQUIRIES]: {
      screen: InquiriesScreen,
      navigationOptions: () => ({
        header: null,
      }),
    },
    [Constants.NAV_NAME_FAQ]: {
      screen: FaqScreen,
      navigationOptions: () => ({
        header: null,
      }),
    },
    [Constants.NAV_NAME_MII_PROFILE]: {
      screen: MiiProfileScreen,
      navigationOptions: () => ({
        header: null,
      }),
    },
    [Constants.NAV_NAME_CATALOG_DETAILS]: {
      screen: CatalogDetailsScreen,
      navigationOptions: () => ({
        header: null,
      }),
    },
  },
  {
    initialRouteName: Constants.NAV_NAME_SPLASH,
  },
);

export default RootNavigation;
