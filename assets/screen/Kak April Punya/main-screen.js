/* eslint-disable import/no-unresolved */
/* eslint-disable react/prefer-stateless-function */
/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import {
  View, Text, StyleSheet, Image, TouchableOpacity, StatusBar,
} from 'react-native';
import * as Constants from '../constants';
import LineSeparator from '../component/lineseparator';
import Footer from '../component/footer';

const menuIcon = require('../assets/icons/ic_menu_drawer.png');
const logo = require('../assets/images/logo_header.png');
const searchIcon = require('../assets/icons/ic_search.png');
const solutionIcon = require('../assets/icons/ic_solution.png');
const productListIcon = require('../assets/icons/ic_product_list.png');
const serviceCalcIcon = require('../assets/icons/ic_service_calculator.png');
const managementDirIcon = require('../assets/icons/ic_management_direction.png');
const newsIcon = require('../assets/icons/ic_news.png');
const challengeInfoIcon = require('../assets/icons/ic_challenge_info.png');
const inquiriesIcon = require('../assets/icons/ic_inquiries.png');
const faqIcon = require('../assets/icons/ic_faq.png');
const miiProfileIcon = require('../assets/icons/ic_mii_profile.png');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffff',
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 100,
    marginHorizontal: 30,
    justifyContent: 'space-between',
  },
  body: {
    flex: 3,
    justifyContent: 'space-evenly',
  },
  sectionStyle: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  contentStyle: {
    width: 105,
    height: 140,
    borderRadius: 10,
    borderWidth: 0,
  },
  centerContent: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  verticalSpacer: {
    marginVertical: 10,
  },
});
export default class MainScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor="#dbdbdb"
          barStyle="dark-content"
        />
        <View style={styles.header}>
          <TouchableOpacity>
            <Image source={menuIcon} />
          </TouchableOpacity>
          <Image source={logo} />
          <TouchableOpacity>
            <Image source={searchIcon} />
          </TouchableOpacity>
        </View>
        <LineSeparator />
        <View style={styles.body}>
          <View style={styles.sectionStyle}>
            <TouchableOpacity
              style={[styles.contentStyle, { backgroundColor: '#CBFFF3' }]}
              onPress={() => this.props.navigation.navigate(Constants.NAV_NAME_SOLUTION)}
            >
              <View style={styles.centerContent}>
                <Image source={solutionIcon} style={styles.verticalSpacer} />
                <Text style={[styles.verticalSpacer, { color: '#2A2A2A' }]}>Solution</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.contentStyle, { backgroundColor: '#FFCBFC' }]}
              onPress={() => this.props.navigation.navigate(Constants.NAV_NAME_PRODUCT_LIST)}
            >
              <View style={styles.centerContent}>
                <Image source={productListIcon} style={styles.verticalSpacer} />
                <Text style={{ color: '#2A2A2A' }}>Product</Text>
                <Text style={{ color: '#2A2A2A' }}>List</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.contentStyle, { backgroundColor: '#CBFFF3' }]}
              onPress={() => this.props.navigation.navigate(Constants.NAV_NAME_SERVICE_CALCULATOR)}
            >
              <View style={styles.centerContent}>
                <Image source={serviceCalcIcon} style={styles.verticalSpacer} />
                <Text style={{ color: '#2A2A2A' }}>Service</Text>
                <Text style={{ color: '#2A2A2A' }}>Calculator</Text>
              </View>
            </TouchableOpacity>
          </View>
          <View style={styles.sectionStyle}>
            <TouchableOpacity
              style={[styles.contentStyle, { backgroundColor: '#FFF3CB' }]}
              onPress={
                () => this.props.navigation.navigate(Constants.NAV_NAME_MANAGEMENT_DIRECTION)
              }
            >
              <View style={styles.centerContent}>
                <Image source={managementDirIcon} style={styles.verticalSpacer} />
                <Text style={{ color: '#2A2A2A' }}>Management</Text>
                <Text style={{ color: '#2A2A2A' }}>Direction</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.contentStyle, { backgroundColor: '#CBFFF3' }]}
              onPress={() => this.props.navigation.navigate(Constants.NAV_NAME_NEWS)}
            >
              <View style={styles.centerContent}>
                <Image source={newsIcon} style={styles.verticalSpacer} />
                <Text style={{ color: '#2A2A2A' }}>Accountant / </Text>
                <Text style={{ color: '#2A2A2A' }}>Finance News</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.contentStyle, { backgroundColor: '#FFCBFC' }]}
              onPress={() => this.props.navigation.navigate(Constants.NAV_NAME_CHALLENGE_INFO)}
            >
              <View style={styles.centerContent}>
                <Image source={challengeInfoIcon} style={styles.verticalSpacer} />
                <Text style={[styles.verticalSpacer, { color: '#2A2A2A' }]}>Challenge Info</Text>
              </View>
            </TouchableOpacity>
          </View>
          <View style={styles.sectionStyle}>
            <TouchableOpacity
              style={[styles.contentStyle, { backgroundColor: '#CBFFF3' }]}
              onPress={() => this.props.navigation.navigate(Constants.NAV_NAME_INQUIRIES)}
            >
              <View style={styles.centerContent}>
                <Image source={inquiriesIcon} style={styles.verticalSpacer} />
                <Text style={[styles.verticalSpacer, { color: '#2A2A2A' }]}>Inquiries</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.contentStyle, { backgroundColor: '#DCFFCB' }]}
              onPress={() => this.props.navigation.navigate(Constants.NAV_NAME_FAQ)}
            >
              <View style={styles.centerContent}>
                <Image source={faqIcon} style={styles.verticalSpacer} />
                <Text style={[styles.verticalSpacer, { color: '#2A2A2A' }]}>FAQ</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.contentStyle, { backgroundColor: '#CBFFF3' }]}
              onPress={() => this.props.navigation.navigate(Constants.NAV_NAME_MII_PROFILE)}
            >
              <View style={styles.centerContent}>
                <Image source={miiProfileIcon} style={styles.verticalSpacer} />
                <Text style={[styles.verticalSpacer, { color: '#2A2A2A' }]}>MII Profile</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
        <Footer />
      </View>

    );
  }
}
