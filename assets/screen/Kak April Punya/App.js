/* eslint-disable react/prefer-stateless-function */
/* eslint-disable import/no-unresolved */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';

import RootNavigation from './src/navigation';

export default class App extends Component {
  render() {
    return (
      <RootNavigation />
    );
  }
}
