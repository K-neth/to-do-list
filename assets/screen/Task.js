/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from 'react';
import {
  StyleSheet, Text, View, TextInput, Button, TouchableHighlight, TimePickerAndroid, Image,
  DatePickerAndroid,
} from 'react-native';
import moment from 'moment';
import PropTypes from 'prop-types';

const time = require('../img/alarm-clock-blue.png');
const date = require('../img/icon-blue.png');

const styles = StyleSheet.create({
  button: {
    backgroundColor: '#4ba37b',
    width: 100,
    borderRadius: 50,
    alignItems: 'center',
    marginTop: 150,
  },
  upperContainer: {
    flex: 1,
    paddingLeft: 25,
    paddingTop: 100,
    paddingRight: 25,
    paddingBottom: 30,
    backgroundColor: '#6666cc',
  },
  bottomContainer: {
    flex: 1,
    paddingLeft: 25,
    paddingTop: 100,
    paddingRight: 25,
    paddingBottom: 30,
    backgroundColor: '#cc6666',
  },
  buttonContainer: {
    borderWidth: 0,
    borderRadius: 10,
    elevation: 10,
    backgroundColor: 'white',
    justifyContent: 'flex-start',
    marginTop: 60,
    marginBottom: 80,
    height: 450,
    position: 'absolute',
    alignSelf: 'center',
    alignItems: 'center',
  },
  txtTaskTime: {
    opacity: 0.5,
    paddingTop: 10,
    paddingRight: 20,
    fontSize: 10,
  },
  dueTimePickerBtn: {
    borderWidth: 0.1,
    alignItems: 'center',
    marginBottom: 20,
  },
  txtInputNewTask: {
    elevation: 1,
    width: 300,
    height: 80,
    textAlign: 'center',
    marginBottom: 20,
    alignSelf: 'flex-start',
  },
  lbldueTime: {
    fontWeight: 'bold',
    marginBottom: 10,
  },
  lblnewTask: {
    fontSize: 30,
    fontWeight: 'bold',
  },
  txtDateAndTime: {
    textAlign: 'center',
    marginBottom: 10,
  },
});

class Task extends Component {
  state = {
    text: '',
    date: '',
    time: '',
  };

  datePicker = async () => {
    const {
      action, year, month, day,
    } = await DatePickerAndroid.open({ date: new Date() });
    if (action !== DatePickerAndroid.dismissedAction) {
      const dateString = `${day} - ${month} - ${year}`;
      this.setState({ date: dateString });
    }
  }

  timePicker = async () => {
    const { action, hour, minute } = await TimePickerAndroid.open({
      hour: 0,
      minute: 0,
      is24Hour: true,
    });
    if (action !== TimePickerAndroid.dismissedAction) {
      if (hour < 10 && minute < 10) {
        await this.setState(() => ({
          time: `0${hour}0${minute}`,
        }));
      } else if (hour < 10) {
        await this.setState(() => ({
          time: `0${hour}${minute}`,
        }));
      } else {
        await this.setState(() => ({
          time: `${hour}${minute}`,
        }));
      }
    }
  }

  onTodoStatusChange = (id) => {
    this.props.onTodoStatusChange(id);
  }

  onPressButton = () => {
    this.props.onPressButton(this.state.text, this.state.date, this.state.time);
    this.props.navigation.goBack();
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={styles.upperContainer} />
        <View style={styles.bottomContainer} />
        <View style={styles.buttonContainer}>
          <Text style={styles.lblnewTask}>Add New Task</Text>
          <TextInput
            style={styles.txtInputNewTask}
            placeholder="Input job description"
            onChangeText={(text) => {
              this.setState({ text });
            }}
          />
          <View>
            <Text style={styles.lbldueTime}>Input due date</Text>
            <Text style={styles.txtDateAndTime}>{this.state.date}</Text>
            <TouchableHighlight
              style={styles.dueTimePickerBtn}
              onPress={() => {
                this.datePicker();
              }}
            >
              <Image
                source={date}
              />
            </TouchableHighlight>
            <Text style={styles.lbldueTime}>Input due time</Text>
            <Text style={styles.txtDateAndTime}>{moment(this.state.time, 'HHmm').format('LT')}</Text>
            <TouchableHighlight
              style={styles.dueTimePickerBtn}
              onPress={() => {
                this.timePicker();
              }}
            >
              <Image
                source={time}
              />
            </TouchableHighlight>
            <Button
              style={styles.button}
              color="#cc6666"
              onPress={this.onPressButton}
              title="Submit"
            />
          </View>
        </View>
      </View>
    );
  }
}

export default Task;

// const todoShape = PropTypes.shape({
//   id: PropTypes.string,
//   taskName: PropTypes.string,
//   taskdueDate: PropTypes.string,
//   taskdueTime: PropTypes.string,
//   checked: PropTypes.bool,
// });

Task.propTypes = {
  onPressButton: PropTypes.func.isRequired,
  onTodoStatusChange: PropTypes.func.isRequired,
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
    getParam: PropTypes.func,
    goBack: PropTypes.func,
  }),
};

Task.defaultProps = {
  navigation: { navigate: () => {} },
};
