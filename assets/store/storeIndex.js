import { createStore, applyMiddleware } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import logger from 'redux-logger';
import storage from 'redux-persist/lib/storage';
import reducersRedux from '../reducer/combinedReducers';

const persistConfig = {
  key: 'root',
  storage,
};

const persistedReducer = persistReducer(persistConfig, reducersRedux);
export const store = createStore(persistedReducer, applyMiddleware(logger));
export const persistor = persistStore(store);


export default () => ({ store, persistor });
