// @flow

import { FETCH_DATA_ERROR } from '../indexAction';

const fetchDataError = () => (
  {
    type: FETCH_DATA_ERROR,
    payload: { error: true },
  }
);

export default fetchDataError;
