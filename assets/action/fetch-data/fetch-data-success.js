// @flow

import { FETCH_DATA_SUCCESS } from '../indexAction';

const fetchDataSuccess = userList => (
  {
    type: FETCH_DATA_SUCCESS,
    payload: { userList },
  }
);

export default fetchDataSuccess;
