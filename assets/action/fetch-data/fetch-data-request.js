// @flow

import { FETCH_DATA_REQUEST } from '../indexAction';

const fetchDataRequest = () => (
  {
    type: FETCH_DATA_REQUEST,
    payload: { isLoading: true },
  }
);

export default fetchDataRequest;
