// @flow
import { fetchUserData } from './services/http-requests';
import { fetchDataError } from './fetch-data-error';
import { fetchDataRequest } from './fetch-data-request';
import { fetchDataSuccess } from './fetch-data-success';

export const fetchData = () => (
  (dispatch) => {
    dispatch(fetchDataRequest());
    return fetchUserData()
      .then(userList => dispatch(fetchDataSuccess(userList)))
      .catch(() => dispatch(fetchDataError()));
  }
);

export default fetchData();
