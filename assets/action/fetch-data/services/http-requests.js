// @flow

import config from '../config';

export const fetchUserData = () => (
  fetch(config.API_URL)
    .then(res => res.json())
    .then(data => data.currently)
    .catch(err => err)
);

export default fetchUserData();
