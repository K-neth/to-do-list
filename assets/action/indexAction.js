
export const ADD_TODO = 'ADD_TODO';
export const ADD_USER = 'ADD_USER';
export const TOGGLE_TODO = 'TOGGLE_TODO';
export const ON_LOAD = 'ON_LOAD';

export const addTodo = (text, date, time) => ({
  type: ADD_TODO,
  text,
  date,
  time,
});

export const toggleTodo = id => ({
  type: TOGGLE_TODO,
  id,
});

export const addUsers = user => ({
  type: ADD_USER,
  user,
});

export const onLoad = detail => ({
  type: ON_LOAD,
  detail,
});
