import { connect } from 'react-redux';
import * as ActionRedux from '../action/indexAction';
import UserDetail from '../screen/UserDetail';

const mapStateToProps = state => ({
  userdetails: state.userdetails,
});

const mapDispatchToProps = dispatch => ({
  onLoad: (detail) => {
    dispatch(ActionRedux.onLoad(detail));
  },
});

const MainContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(UserDetail);

export default MainContainer;
