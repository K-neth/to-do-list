import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { View } from 'react-native';
import { Button } from 'react-native-elements';
import { TextInput } from 'react-native-gesture-handler';
import { addTodo } from '../action/indexAction';

const AddTodo = ({ dispatch }) => {
  let input;

  return (
    <View>
      <TextInput ref={(node) => { (input = node); }} />
      <Button onPress={(e) => {
        e.preventDefault();
        if (!input.value.trim()) {
          return;
        }
        dispatch(addTodo(input.value));
        input.value = '';
      }}
      >
        Add Todo
      </Button>
    </View>
  );
};

AddTodo.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

export default connect()(AddTodo);
