import { connect } from 'react-redux';
import Task from '../screen/Task';
import * as ActionRedux from '../action/indexAction';

const mapDispatchToProps = dispatch => ({
  onTodoStatusChange: (id) => {
    dispatch(ActionRedux.ToggleTodo(id));
  },
  onPressButton: (text, date, time) => {
    dispatch(ActionRedux.addTodo(text, date, time));
  },
});

const MainContainer = connect(
  null,
  mapDispatchToProps,
)(Task);

export default MainContainer;
