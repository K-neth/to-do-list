import { connect } from 'react-redux';
import User from '../screen/User';
import * as ActionRedux from '../action/indexAction';

const mapStateToProps = state => ({
  users: state.users,
});

const mapDispatchToProps = dispatch => ({
  AddUsers: (user) => {
    dispatch(ActionRedux.addUsers(user));
  },
});

const MainContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(User);

export default MainContainer;
