import { connect } from 'react-redux';
import Home from '../screen/Home';
import * as ActionRedux from '../action/indexAction';

// const getFilteredTodos = (listOfTodos, filter) => {
//   switch (filter) {
//     case ActionRedux.StatusFilter.SHOW_ALL:
//       return listOfTodos;
//     case ActionRedux.StatusFilter.UNCOMPLETED:
//       return listOfTodos.filter(todo => todo.isCompleted === false);
//     case ActionRedux.StatusFilter.COMPLETED:
//       return listOfTodos.filter(todo => todo.isCompleted === true);
//     default:
//       return listOfTodos;
//   }
// };

const mapStateToProps = state => ({
  homes: state.homes,
});

const mapDispatchToProps = dispatch => ({
  onTodoStatusChange: (id) => {
    dispatch(ActionRedux.toggleTodo(id));
  },
  onPressButton: (text, date, time) => {
    dispatch(ActionRedux.addTodo(text, date, time));
  },
});

const MainContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Home);

export default MainContainer;
