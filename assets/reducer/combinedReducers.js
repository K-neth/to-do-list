import { combineReducers } from 'redux';
import homes from './homes';
import users from './users';
import userdetails from './userdetails';

export const getUserSelector = state => ({ ...state.user });

const reducersRedux = combineReducers({
  homes,
  userdetails,
  users,
});

export default reducersRedux;
