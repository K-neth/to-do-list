import uuid from 'uuid/v4';
import * as ActionRedux from '../action/indexAction';

const homes = (state = [], action) => {
  switch (action.type) {
    case ActionRedux.ADD_TODO:
      return [
        ...state,
        {
          id: uuid(),
          taskName: action.text,
          taskdueDate: action.date,
          taskdueTime: action.time,
          checked: false,
        },
      ];
    case ActionRedux.TOGGLE_TODO:
      return state.map((todo) => {
        if (todo.id === action.id) {
          return {
            ...todo,
            checked: !todo.checked,
          };
        }
        return todo;
      });
    default:
      return state;
  }
};

export default homes;
