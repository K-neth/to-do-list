import * as ActionRedux from '../action/indexAction';

const userdetails = (state = {}, action) => {
  switch (action.type) {
    case ActionRedux.ON_LOAD:
      return {
        ...state, detail: action.detail,
      };
    default:
      return state;
  }
};

export default userdetails;
