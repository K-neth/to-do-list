import * as ActionRedux from '../action/indexAction';

const users = (state = [], action) => {
  switch (action.type) {
    case ActionRedux.ADD_USER:
      return [
        ...state, action.user,
      ];
    default:
      return state;
  }
};

export default users;
